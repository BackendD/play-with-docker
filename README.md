# Play With Docker

Initiate the container for:

- Browsing files
- Runnig torrents
- Downloading from yotube and other media

## Execute

Run the following command in the terminal:

```sh
curl -sLf https://pwd.gitme.cf/init.sh | bash; tmux a;
```

## What is done?

### Running a file browser server:

Using [this image](https://hub.docker.com/r/filebrowser/filebrowser). The port number is: **80**.

### Running a qBittorrent Client:

Using [this image](https://hub.docker.com/r/wernight/qbittorrent). The web UI port number is: **8080**.

### Alias a youtube downloader command:

You can use `yt-dlp` to download media, for example:

```sh
yt-dlp -f 'bestvideo[height<=1080p]+bestaudio' --write-auto-sub --write-sub --sub-lang en -a y.txt
```